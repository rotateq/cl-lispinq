;;;; arm.lisp

(declaim (ftype (function ((integer 0)) simple-string) integer->string))
(defun integer->string (n)
  "Convert an integer N >= 0 to its string form."
  ;; (check-type n (integer 0))
  (cond ((zerop n)
         "0")
        (t 
         (let* ((length (ceiling (log n 10)))
                (string (make-string length :initial-element #\Nul)))
           (multiple-value-bind (d m) (truncate n 10)
             (loop :for i :downfrom (1- length) :to 0
                   :do (setf (schar string i) (code-char (+ m 48)))
                   :do (multiple-value-setq (d m) (truncate d 10))
                   :finally (return-from integer->string string)))))))

(define-compiler-macro integer->string (&whole form n)
  (cond ((integerp n)
         (cond ((zerop n) '"0")
               (t form)))
        (t
         form)))
