(defun symbol->keyword (symbol)
  (declare (type symbol symbol))
  (if (keywordp symbol)
      symbol
      (read-from-string (uiop:strcat #\: (string symbol)))))

(defmacro define-struct (struct-name &rest slot-definitions)
  (let ((conc-name (uiop:strcat (string struct-name) #\-)))
    (labels ((convert-struct-slot (slot-definition)
               (etypecase slot-definition
                 (symbol (let* ((slot-name slot-definition)
                                (initarg (symbol->keyword slot-name))
                                (accessor (alexandria:symbolicate conc-name (string slot-name))))
                           `(,slot-name :initarg ,initarg
                                        :accessor ,accessor)))
                 (list (ecase (length slot-definition)
                         (1 (convert-struct-slot (car slot-definition)))
                         (2 (let ((slot-name (car slot-definition))
                                  (initform (cadr slot-definition)))
                              (append (convert-struct-slot slot-name)
                                      `(:initform ,initform))))
                         ((or 4 6)
                          (let* ((keyword-list (cddr slot-definition))
                                 (body (convert-struct-slot
                                        (list (car slot-definition)
                                              (cadr slot-definition))))
                                 (rest (destructuring-bind (&key type read-only) keyword-list
                                         (list type read-only)))
                                 (final-body (append body `(:type ,(car rest)))))
                            (if (cadr rest)
                                (sublis '((:accessor . :reader)) final-body :test #'eq)
                                final-body))))))))
      (let* ((constructor-name (alexandria:symbolicate 'make- struct-name))
             (lambda-list-symbols (loop :for slot :in slot-definitions
                                        :if (symbolp slot)
                                          :collect slot
                                        :else :collect (car slot)))
             (lambda-list-keys (mapcar #'symbol->keyword lambda-list-symbols))
             (constructor-lambda-list `(&key ,@lambda-list-symbols))
             (key-symbol-pairs (loop :for key :in lambda-list-keys
                                     :for sym :in lambda-list-symbols
                                     :collect key :into final-list
                                     :collect sym :into final-list
                                     :finally (return final-list))))
        `(progn
           (defclass ,struct-name ()
             ,(mapcar #'convert-struct-slot slot-definitions))
           (defun ,constructor-name ,constructor-lambda-list
             (make-instance ',struct-name ,@key-symbol-pairs)))))))
