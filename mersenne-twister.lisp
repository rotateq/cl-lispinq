;;;; This is the translation of C code.
;;;; https://de.wikipedia.org/wiki/Mersenne-Twister

;;; Maybe change the following two to constants later.
;;; better use A:DEFINE-CONSTANT for that
(defvar *N* 624)
(defvar *M* 397)

(defun |$-reader| (stream char)
  "Reads in the stream as a string until an enclosing #\$ appears."
  (declare (ignore char))
  (let ((chr #\nul)
        (chars '()))
    (loop named read-loop
          do (setq chr (read-char stream))
          if (char= chr #\/)
            do (return-from read-loop)
          else
            do (push chr chars))
    (make-array (length chars) :element-type 'character :initial-contents chars)))

(defun mersenne-twister-vector-init (p len)
  (let ((mult 1812433253)
	      (seed 5489))
    (loop for i below len
	        do (symbol-macrolet ((p[i] (aref p i)))
	             (setq p[i] seed
		                 seed (+ i 1 (* mult (logxor seed (ash seed -30)))))))))

(defun mersenne-twister-vector-update (p)
  (let ((A #(0 #x9908b0df)))
    (symbol-macrolet ((p[i] (aref p i))
                      (p[i+1] (aref p (1+ i)))
                      (A[p[i+1]&1] (aref A (logand p[i+1] 1))))
      (loop for i below (- *N* *M*)
	          do (symbol-macrolet ((p[i+M] (aref p (+ i *M*))))
                 (setq p[i] (logxor p[i+M] A[p[i+1]&1]
				                            (logior (logand p[i] #x80000000)
	                                          (ash (logand p[i+1] #x7fffffff) -1))))))
      (loop for i below (1- *N*)
	          do (symbol-macrolet ((p[i+M-N] (aref p (+ i *M* (- *N*)))))
                 (setq p[i] (logxor p[i+M-N] A[p[i+1]&1]
				                            (logior (logand p[i] #x80000000)
					                                  (ash (logand p[i+1] #x7fffffff) -1)))))))
    (symbol-macrolet ((p[N-1] (aref p (1- *N*)))
                      (p[M-1] (aref p (1- *M*)))
                      (p[0] (aref p 0))
                      (A[p[0]&1] (aref A (logand p[0] 1))))
      (setq p[N-1] (logxor p[M-1] A[p[0]&1]
			                     (logior (logand p[N-1] #x80000000)
				                           (ash (logand p[0] #x7fffffff) -1)))))))

(defun mersenne-twister ()
  (let ((state-vector (make-array *N*))
	      (index (1+ *N*))
	      (e 0))
    (when (>= index *N*)
      (when (> index *N*)
	      (mersenne-twister-vector-init state-vector *N*))
      (mersenne-twister-vector-update state-vector)
      (setq index 0))
    (setq e (aref state-vector index))
    (incf index)
    (macrolet ((e^= (expr)
		             `(setq e (logxor e ,expr))))
      (e^= (ash e -11))
      (e^= (logand (ash e 7) #x9d2c5680))
      (e^= (logand (ash e 15) #xefc60000))
      (e^= (ash e -18)))
    (return-from mersenne-twister e)))
