;;;; singleton.lisp

;;;; This is to show how a singleton class could be implemented in CLOS.

;;; First create the class SINGLETON-CLASS for using it as a metaclass.

(defclass singleton-class (standard-class)
  ((%counter :initform 0
             :type bit)))

;;; It must be validated.

(defmethod closer-mop:validate-superclass ((class singleton-class) (superclass standard-class))
  t)

(defclass singleton-object ()
  ()
  (:metaclass singleton-class))

(defmacro defsingleton (name superclasses slots)
  `(defclass ,name (singleton-object ,@superclasses)
     ,slots
     (:metaclass singleton-class)))
