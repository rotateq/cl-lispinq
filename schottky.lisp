(deftype cd () '(complex double-float))

(deftype moebius () '(simple-array cd (4)))

(defun make-moebius (a b c d)
  (assert (every #'numberp (list a b c d)))
  (setq a (coerce a 'cd)
        b (coerce b 'cd)
        c (coerce c 'cd)
        d (coerce d 'cd))
  (make-array 4 :element-type 'cd
                :initial-contents
                (list a b c d)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun |#m-reader| (stream char numarg)
    "This is for reading in Möbius transforms.
     All entries must be numbers."
    (declare (ignore char numarg))
    (read-char stream)
    (let ((args (read-delimited-list #\) stream)))
      `(apply #'make-moebius ',args))))

(set-dispatch-macro-character #\# #\m #'|#m-reader|)

(defmacro with-moebius (bindings moebius &body body)
  (assert (= 4 (length bindings)))
  (let ((blist (loop :for sym :in bindings
                     :for i :from 0 :to 3
                     :collect `(,sym (aref ,moebius ,i)))))
    `(symbol-macrolet ,blist
       ,@body)))

(declaim (ftype (function (moebius cd) cd) apply-moebius))
(defun apply-moebius (m z)
  (with-moebius (a b c d) m
    (/ (+ (* a z) b)
       (+ (* c z) d))))

(defun mempty ()
  (make-moebius 1 0 0 1))

(defun mappend (m1 m2)
  (with-moebius (a1 b1 c1 d1) m1
    (with-moebius (a2 b2 c2 d2) m2
      (make-moebius (+ (* a1 a2)
                       (* b1 c2))
                    (+ (* a1 b2)
                       (* b1 d2))
                    (+ (* c1 a2)
                       (* d1 c2))
                    (+ (* c1 b2)
                       (* d1 d2))))))

(defun inverse (m)
  (with-moebius (a b c d) m
    (make-moebius d (- b) (- c) a)))

(deftype ctuple () '(cons cd cd))

(defun det (m)
  (with-moebius (a b c d) m
    (- (* a d)
       (* b c))))

(defun fixpoints (m)
  (with-moebius (a b c d) m
    (solve-quadratic c (- d a) (- b))))

(defun fixpoint-attractive (m)
  (with-moebius (a b c d) m
    (let* ((fixpoints (fixpoints m))
           (z1 (car fixpoints))
           (z2 (cdr fixpoints))
           (multiplier (/ (- a (* c z1))
                          (- a (* c z2)))))
      (if (>= (abs multiplier) 1)
          z2
          z1))))

(deftype letter () '(member A B A1 B1))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun letterp (x)
    (when (symbolp x)
      (typep x 'letter)))

  (defun wordp (w)
    (when (listp w)
      (every #'letterp w))))

(deftype word () '(and list (satisfies wordp)))

(defmethod next ((obj (eql 'A)))
  '(B1 A B))

(defmethod next ((obj (eql 'B1)))
  '(A1 B1 A))

(defmethod next ((obj (eql 'A1)))
  '(B A1 B1))

(defmethod next ((obj (eql 'B)))
  '(A B A1))

(setf (fdefinition 'left) #'first)
(setf (fdefinition 'middle) #'second)
(setf (fdefinition 'right) #'third)

(defun commutator-left (x)
  (flet ((left-next (a:compose #'left #'next)))
    (loop :with xs := (list x)
          :repeat 3
          :do (setq x (left-next x))
              (push x xs)
          :finally (return-from commutator-left (reverse xs)))))

(defun commutator-right (x)
  (flet ((right-next (a:compose #'right #'next)))
    (loop :with xs := (list x)
          :repeat 3
          :do (setq x (right-next x))
              (push x xs)
          :finally (return-from commutator-right (reverse xs)))))

