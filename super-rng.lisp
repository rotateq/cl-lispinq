;;;; super-rng.lisp
;;;
;;; Algorithm K from TAoCP book 2, chapter 3, section 3.1
;;;

(defun algorithm-k (X)
  "(\"Super-random\" number generator). Given a 10-digit decimal
number X, this algorithm may be used to change X to the number that should
come next in a supposedly random sequence. Although the algorithm might be
expected to yield quite a random sequence, reasons given below show that it
is not, in fact, very good at all. (The reader need not study this algorithm in
great detail except to observe how complicated it is; in particular, steps
K1 and K2.)"
  (flet ((decrease-number-digitwise (n)
           (loop with m = 0
                 while (and (> n 0) (>= m 0))
                 for i from 0
                 do (multiple-value-setq (n m) (truncate n 10))
                 sum (if (zerop m) 0 (* (1- m) (expt 10 i))) into s
                 finally (return-from decrease-number-digitwise s))))
    (let* ((10^5 (expt 10 5))
           (10^8 (expt 10 8))
           (10^9 (expt 10 9))
           (10^10 (expt 10 10))
           (5*10^9 (* 5 10^9))
           (Y 0) (Z 0))
      (tagbody
       K1 ; [Choose number of iterations.]
        (setq Y (truncate X 10^9))
       K2 ; [Choose random step.]
        (setq Z (mod (truncate X 10^8) 10))
        (cond ((= Z 0) (go K3))
              ((= Z 1) (go K4))
              ((= Z 2) (go K5))
              ((= Z 3) (go K6))
              ((= Z 4) (go K7))
              ((= Z 5) (go K8))
              ((= Z 6) (go K9))
              ((= Z 7) (go K10))
              ((= Z 8) (go K11))
              ((= Z 9) (go K12)))
       K3 ; [Ensure ≥ 5×10^9.]
        (when (< X 5*10^9)
          (incf X 5*10^9))
       K4 ; [Middle square.]
        (setq X (mod (truncate (* X X) 10^5) 10^10))
       K5 ; [Multiply.]
        (setq X (mod (* 1001001001 X) 10^10))
       K6 ; [Pseudo-complement.]
        (if (< X 10^8)
            (incf X 9814055677)
            (setq X (- 10^10 X)))
       K7 ; [Interchange halves.]
        (setq X (+ (* 10^5 (mod X 10^5)) (truncate X 10^5)))
       K8 ; [Multiply.]
        (setq X (mod (* 1001001001 X) 10^10))
       K9 ; [Decrease digits.]
        (setq X (decrease-number-digitwise X))
       K10 ; [99999 modify.]
        (if (< X 10^5)
            (setq X (+ (* X X) 99999))
            (decf X 99999))
       K11 ; [Normalize.]
        (when (< X 10^9)
          (setq X (* 10 X))
          (go K11))
       K12 ; [Modified middle square.]
        (setq X (mod (floor (/ (* X (1- X)) 10^5)) 10^10))
       K13 ; [Repeat?]
        (if (> Y 0)
            (progn
              (decf Y)
              (go K2))
            (return-from algorithm-k X))))))
