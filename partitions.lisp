;; (declaim (ftype (function ((and fixnum (integer 0))) (integer 1)) partition))
(defun partition (n)
  "Calculate the N-th partition, in Wolfram Language called PartitionsP[n]."
  (let ((partition-lookup-table (make-array (1+ n) :element-type '(integer 0))))
    (symbol-macrolet ((partition[i] (svref partition-lookup-table i)))
      (loop for i below 11
            for p across (the (simple-vector 11) #(1 1 2 3 5 7 11 15 22 30 42))
            do (setq partition[i] p))
      (labels ((%partition (k)
                 "Our closure for the recurrence formula by Euler."
                 ;; We assume certain entry conditions hold for K:
                 ;; K is in the range [0;N] so that a lookup cannot have bad index.
                 ;; If an entry is 0 it's not calculated yet.
                 (symbol-macrolet ((q+ (- k (* 1/2 m (1+ (* 3 m)))))
                                   (q- (- k (* 1/2 m (1- (* 3 m)))))
                                   (partition[q+] (svref partition-lookup-table q+))
                                   (partition[q-] (svref partition-lookup-table q-)))
                   (loop for m from 1
                         while (>= q+ 0)
                         sum (* (expt -1 (1+ m)) (+ partition[q+] partition[q-]))
                           into %loop-sum
                         finally (unwind-protect
                                   (when (>= q- 0)
                                     (incf %loop-sum (* (expt -1 (1+ m)) partition[q-])))
                                   (return-from %partition %loop-sum))))))
        (loop for i from 11 below n
              do (setf partition-lookup-table[i] (%partition i))
              finally (return-from partition (%partition n)))))))