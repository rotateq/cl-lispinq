;;;; bitfield.lisp

(defclass bitfield ()
  ((%length :initarg :length
            :initform 0
            :reader bitfield-length
            :type (integer 0)
            :documentation "How can a length of such object be negative?")
   (%content :type (simple-array (unsigned-byte 8) (*))))
  (:documentation "The BITFIELD class is for representing bitvectors in another way.
                   We use vectors of unboxed 8-bit unsigned integers.
                   This maps well to hardware in terms of standardizes 8-bit bytes.
                   In general an object of type BITFIELD has adjustable length."))

(defclass simple-bitfield (bitfield)
  ()
  (:documentation "An object of type SIMPLE-BITFIELD has fixed length, similar to SIMPLE-BIT-VECTOR."))

(defmethod initialize-instance :after ((object simple-bitfield) &key)
  "Bind an unboxed array of 8-bit integers to the %CONTENT slot of object that fits
   to the %LENGTH slot given before when MAKE-INSTANCE was called."
  (with-slots (%length %content) object
    (multiple-value-bind (d m) (truncate %length 8)
      (let ((dimension (if (zerop m) d (1+ d))))
        (setq %content (make-array dimension :element-type '(unsigned-byte 8)))))))

(defun make-bitfield (length)
  "A wrapper to have it like in MAKE-ARRAY and save a keyword."
  (make-instance 'bitfield :length length))

(defgeneric bbit (bitfield index)
  (:documentation "Get the bit in BITFIELD at INDEX. Similar to SBIT."))

(defgeneric (setf bbit) (new-value bitfield index)
  (:documentation "Set the bit in BITFIELD at INDEX. Similar to (SETF SBIT)."))

(defmethod bbit :before ((object bitfield) (index integer))
  (check-type index (integer 0))
  (with-slots (%length) object
    (assert (< index %length))))

(defmethod bbit ((object bitfield) (index integer))
  "Get the value of the bit at INDEX in OBJECT of type BITFIELD."
  (with-slots (%length %content) object
    (multiple-value-bind (d m) (truncate index 8)
      (ldb (byte 1 m) (aref %content d)))))

(defmethod (setf bbit) :before ((new-value integer) (object bitfield) (index integer))
  (check-type new-value bit)
  (check-type index (integer 0))
  (with-slots (%length) object
    (assert (< index %length))))

(defmethod (setf bbit) ((new-value integer) (object bitfield) (index integer))
  "Set the value of the bit at INDEX in OBJECT of type BITFIELD."
  (with-slots (%length %content) object
    (multiple-value-bind (d m) (truncate index 8)
      (setf (ldb (byte 1 m) (aref %content d)) new-value))))

;;; please ignore that for now, and even if not, it won't fire the missiles
#+(or)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun bitfield-reader (stream char numarg)
    (declare (ignore char numarg))
    (let* ((char (read-char stream nil #\Space))
           (length 0)
           (bits (loop until (case char
                               (#\Space t)
                               (#\Tab t)
                               (#\Newline t)
                               (#\) t)
                               (otherwise nil))
                       when (char= #\0 char)
                         collect 0
                       when (char= #\1 char)
                         collect 1
                       do (incf length)
                       do (setq char (read-char stream nil #\Space))))
           (bitfield (make-bitfield length)))
      (loop for bit in bits
            for index below length
            do (setf (bbit bitfield index) bit)
            finally (return-from bitfield-reader bitfield))))

  (set-dispatch-macro-character #\# #\~ #'bitfield-reader))
