(defclass ellipse ()
  ((h-axis :initarg :h-axis
           :accessor h-axis
           :type real)
   (v-axis :initarg :v-axis
           :accessor v-axis
           :type real)))

(defclass circle (ellipse)
  ((radius :initarg :radius
           :accessor radius
           :type real)))

;;;
;;; A circle has a radius, but also a h-axis and v-axis that
;;; it inherits from an ellipse. These must be kept in sync
;;; with the radius when the object is initialized and
;;; when those values change.
;;;
(defmethod initialize-instance ((c circle) &key radius)
  (setf (radius c) radius)) ;; via the setf method below

(defmethod (setf radius) :after ((new-value real) (c circle))
  (setf (slot-value c 'h-axis) new-value
        (slot-value c 'v-axis) new-value))

;;;
;;; After an assignment is made to the circle's
;;; h-axis or v-axis, a change of type is necessary,
;;; unless the new value is the same as the radius.
;;;
(defmethod (setf h-axis) :after ((new-value real) (object circle))
  (unless (= (radius object) new-value)
    (change-class object 'ellipse)))

(defmethod (setf v-axis) :after ((new-value real) (object circle))
  (unless (= (radius object) new-value)
    (change-class object 'ellipse)))

;;;
;;; Ellipse changes to a circle if accessors
;;; mutate it such that the axes are equal,
;;; or if an attempt is made to construct it that way.
;;;
;;; EQL equality is used, under which 0 /= 0.0.
;;;
;;;
(defmethod initialize-instance :after ((object ellipse) &key h-axis v-axis)
  (if (= h-axis v-axis)
    (change-class object 'circle)))

(defmethod (setf h-axis) :after ((new-value real) (object ellipse))
  (unless (typep object 'circle)
    (if (= (h-axis object) (v-axis object))
      (change-class object 'circle))))

(defmethod (setf v-axis) :after ((new-value real) (e ellipse))
  (unless (typep e 'circle)
    (if (= (h-axis e) (v-axis e))
      (change-class e 'circle))))

;;;
;;; Method for an ellipse becoming a circle. In this metamorphosis,
;;; the object acquires a radius, which must be initialized.
;;; There is a "sanity check" here to signal an error if an attempt
;;; is made to convert an ellipse which axes are unequal
;;; with an explicit change-class call.
;;; The handling strategy here is to base the radius off the
;;; h-axis and signal an error.
;;; This doesn't prevent the class change; the damage is already done.
;;;
(defmethod update-instance-for-different-class :after ((old-e ellipse)
                                                       (new-c circle) &key)
  (setf (radius new-c) (h-axis old-e))
  (unless (= (h-axis old-e) (v-axis old-e))
    (error "Ellipse ~s can't change into a circle because it's not one!"
           old-e)))
