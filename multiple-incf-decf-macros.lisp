(cl:in-package :cl-user)

(defmacro multiple-incf-1 (&rest places)
  "Increase all PLACES by 1."
  (cond ((null places)
         `(values))
        ((every #'symbolp places)
         `(setq ,@(loop for place in places
                        collect place
                        collect `(1+ ,place))))
        (t
         `(progn
            ,@(loop for place in places
                    collect `(incf ,place))))))

(defmacro multiple-incf ((&rest places) &optional (delta 1))
  "Increase all PLACES by the same DELTA."
  `(progn
     ,@(loop for place in places
             collect (if (eql delta 1)
                         `(incf ,place)
                         `(incf ,place ,delta)))))

(defmacro multiple-decf-1 (&rest places)
  "Decrease all PLACES by 1."
  (cond ((null places)
         `(values))
        ((every #'symbolp places)
         `(setq ,@(loop for place in places
                        collect place
                        collect `(1- ,place))))
        (t
         `(progn
            ,@(loop for place in places
                    collect `(decf ,place))))))

(defmacro multiple-decf ((&rest places) &optional (delta 1))
  "Decrease all PLACES by the same DELTA."
  `(progn
     ,@(loop for place in places
             collect (if (eql delta 1)
                         `(decf ,place)
                         `(decf ,place ,delta)))))
