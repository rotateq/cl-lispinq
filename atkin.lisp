(defparameter *n-inv* 16)
(defparameter *n-first-primes* 17)

(defparameter *z60-invertibles*
  (make-array *n-element* :inv-type '(integer 1 59)
                    :initial-contents '(1 7 11 13 17 19 23 29 31 37 41 43 47 49 53 59)))
(defparameter *first-primes*
  (make-array *n-first-primes* :element-type '(integer 2 59)
                             :initial-contents '(2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59)))

(defun get-primes (limit)
  (let ((primes (make-array limit :element-type 'bit))
        (n 0)
        (is-prime 0))
    (declare (type simple-bit-vector primes)
             (type bit is-prime))
    (loop :for i :below *n-first-primes*
          :do (setf (sbit primes (aref *first-primes* i)) 1))
    (loop :for w :from 1 :to (1- (ceiling (/ limit 60)))
          :do (loop :for s :below *n-inv*
                    :do (symbol-macrolet ((inv[s] (aref *z60-invertibles* s)))
                          (setq n (+ (* 60 w) inv[s]))
                          (setq is-prime 0)
                          (let ((limit-x 0)
                                (limit-y 0))
                            ;(declare (dynamic-extent limit-x limit-y)
                                        ;        (type (integer 0) limit-x limit-y))
                            (declare (ignorable limit-y))
                            (cond ((= 1 (mod inv[s] 4))
                                   (setq limit-x (ceiling (/ (sqrt n) 2))
                                         limit-y (ceiling (sqrt n)))
                                   (loop :for x :from 1 :to limit-x
                                         :do (loop :for y :from 1 :to limit-y
                                                   :when (= n (+ (* 4 x x) (* y y)))
                                                     :do (setq is-prime (mod (1+ is-prime) 2)))))
                                  ((= 1 (mod inv[s] 6))
                                   (setq limit-x (ceiling (/ (sqrt n) 3))
                                         limit-y (ceiling (sqrt n)))
                                   (loop :for x :from 1 :to limit-x
                                         :do (loop :for y :from 1 :to limit-y
                                                   :when (= n (+ (* 3 x x) (* y y)))
                                                     :do (setq is-prime (mod (1+ is-prime) 2)))))
                                  ((= 11 (mod inv[s] 12))
                                   (setq limit-x n)
                                   (loop :for x :from 2 :to limit-x
                                         :do (loop :for y :from 1 :to (1- x)
                                                   :when (= n (- (* 3 x x) (* y y)))
                                                     :do (setq is-prime (mod (1+ is-prime) 2))))))
                            (loop :for q :from 2 :below (sqrt n)
                                  :while (= 1 is-prime)
                                  :when (and (= 1 (sbit primes q))
                                             (zerop (mod n (* q q))))
                                    :do (setq is-prime 0))
                            (when (= 1 is-prime)
                              (setf (sbit primes n) 1)))))
          :finally (return-from get-primes primes))))
